const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const pool = require("../models/db");
const { JWT_SECRET } = require("../utils/constants");

async function register(req, res, next) {
  const { name, email, password } = req.body;
  try {
    const [rows, fields] = await pool.query(
      "SELECT * FROM users WHERE email = ?",
      [email]
    );
    if (rows.length) {
      return res.status(400).json({ message: "Email already exists" });
    }
    const hashedPassword = await bcrypt.hash(password, 10);
    const [result] = await pool.query(
      "INSERT INTO users (name, email, password) VALUES (?, ?, ?)",
      [name, email, hashedPassword]
    );
    const userId = result.insertId;
    const token = jwt.sign({ userId }, JWT_SECRET, { expiresIn: "1d" });
    res.json({ token });
  } catch (error) {
    next(error);
  }
}

async function login(req, res, next) {
  const { email, password } = req.body;
  try {
    const [rows, fields] = await pool.query(
      "SELECT * FROM users WHERE email = ?",
      [email]
    );
    if (!rows.length) {
      return res.status(400).json({ message: "Invalid email or password" });
    }
    const user = rows[0];
    const isValidPassword = await bcrypt.compare(password, user.password);
    if (!isValidPassword) {
      return res.status(400).json({ message: "Invalid email or password" });
    }
    const token = jwt.sign({ userId: user.id }, JWT_SECRET, {
      expiresIn: "1d",
    });
    res.json({ token });
  } catch (error) {
    next(error);
  }
}

module.exports = { register, login };
