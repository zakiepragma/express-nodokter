const mysql = require("mysql2");
const {
  DB_HOST,
  DB_USER,
  DB_PASSWORD,
  DB_NAME,
} = require("../utils/constants");

const connection = mysql.createConnection({
  host: DB_HOST,
  user: DB_USER,
  password: DB_PASSWORD,
});

const createDatabase = () => {
  connection.connect((err) => {
    if (err) throw err;
    console.log("Connected to MySQL");

    const sql = `CREATE DATABASE IF NOT EXISTS ${DB_NAME}`;

    connection.query(sql, (err, result) => {
      if (err) throw err;
      console.log(`Database ${DB_NAME} created`);
    });
  });
};

const createTableUsers = () => {
  connection.connect((err) => {
    if (err) throw err;
    console.log("Connected to MySQL database");

    const sqlUseDB = `USE ${DB_NAME}`;
    const sqlCreateTableUsers = `CREATE TABLE IF NOT EXISTS users (
          id INT AUTO_INCREMENT PRIMARY KEY,
          name VARCHAR(255) NOT NULL,
          email VARCHAR(255) NOT NULL,
          password VARCHAR(255) NOT NULL,
          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
          updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
        )`;

    connection.query(sqlUseDB, (err, result) => {
      if (err) throw err;
      console.log(`Selected database ${DB_NAME}`);
    });

    connection.query(sqlCreateTableUsers, (err, result) => {
      if (err) throw err;
      console.log("Table users created");
    });
  });
};

module.exports = { createDatabase, createTableUsers };
