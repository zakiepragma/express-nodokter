const express = require("express");
const authRoutes = require("./routes/authRoutes");
const app = express();
const cors = require("cors");
const port = 4000;

const { createDatabase, createTableUsers } = require("./database");

createDatabase(); // buat database
createTableUsers(); // buat tabel users

// enable CORS
app.use(cors());

// app.use(
//   cors({
//     origin: `http://localhost:${port}`,
//   })
// );

// Allow all domains to access the server
// app.use(function (req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header(
//     "Access-Control-Allow-Headers",
//     "Origin, X-Requested-With, Content-Type, Accept"
//   );
//   next();
// });

// Middleware untuk mengizinkan body request berupa JSON
app.use(express.json());

// Menentukan route-endpoint untuk operasi-authentication
app.use("/auth", authRoutes);

// Handler error untuk route yang tidak ditemukan
app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

// Handler error untuk error pada server
app.use((error, req, res, next) => {
  res.status(error.status || 500).json({
    error: {
      message: error.message,
    },
  });
});

// Memulai server
app.listen(port, () => {
  console.log(`Server started at http://localhost:${port}`);
});
